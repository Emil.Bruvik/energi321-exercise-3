#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 15:19:12 2024

@author: ech022
"""

# Placeholder functions (to be implemented based on MATLAB functions)
import numpy as np

def Geometry(DATA, I2, I3):
    NN = DATA['NHShoe'][I3]
    C = DATA['Chord']
    AE = DATA['AE'][I2]

    Span = AE * C
    NNodes = NN + 1
    DY = Span / NN

    HSXYZ = np.zeros((NNodes, 3))
    HSXYZ_R = np.zeros_like(HSXYZ)
    CP = np.zeros((NN, 3))
    Normall = np.zeros_like(CP)

    for i in range(NNodes):
        HSXYZ[i] = [0.25 * C, -Span/2 + DY*i, 0.0]
        HSXYZ_R[i] = [4 * C, -Span/2 + DY*i, 0.0]

    for i in range(NN):
        CP[i] = [0.75 * C, -Span/2 + DY*i + DY/2, 0.0]
        Normall[i] = [0, 0, 1]

    WING = {
        'XYZ': HSXYZ,
        'XYZF': HSXYZ_R,
        'CP': CP,
        'Normall': Normall,
        'NHShoe': NN,
        'Area': Span * C
    }

    return WING



def AerodynamicMatrix(DATA, WING):
    NN = WING['NHShoe']
    A = np.zeros((NN, NN))
    pi = np.pi

    for i in range(NN):
        PointX = WING['CP'][i, 0]
        PointY = WING['CP'][i, 1]

        for j in range(NN):
            x1 = WING['XYZ'][j, 0]
            x2 = WING['XYZ'][j + 1, 0]
            y1 = WING['XYZ'][j, 1]
            y2 = WING['XYZ'][j + 1, 1]

            term1 = -1.0 / (4 * pi * (PointY - y1)) * (1 + np.sqrt((PointX - x1)**2 + (PointY - y1)**2) / (PointX - x1))
            term2 = 1.0 / (4 * pi * (PointY - y2)) * (1 + np.sqrt((PointX - x1)**2 + (PointY - y2)**2) / (PointX - x1))
            
            A[i, j] = term1 + term2

    return A



def RightHandSide(DATA, WING, I1):
    NN = WING['NHShoe']

    # Creating the velocity vector VV
    VV = np.array([1, 0, 0]) * DATA['VInf']
    AoA = DATA['AoA'][I1] * np.pi / 180

    # Transformation matrix T2
    T2 = np.array([
        [np.cos(AoA), 0, np.sin(AoA)],
        [0, 1, 0],
        [-np.sin(AoA), 0, np.cos(AoA)]
    ])

    # Calculating the normal velocity vector VVN
    VVN = np.transpose(T2) @ VV

    # Initializing the RHS array
    RHS = np.zeros(NN)

    # Calculating the RHS values
    for i in range(NN):
        RHS[i] = -np.dot(VVN, WING['Normall'][i, :3])

    return RHS

def AerodynamicLoads(WING, DATA, GAMMA):
    NN = WING['NHShoe']
    pi = np.pi

    b = np.zeros((NN, NN))
    D = np.zeros(NN)
    L = np.zeros(NN)

    for i in range(NN):
        X = WING['CP'][i, 0]
        Y = WING['CP'][i, 1]

        for j in range(NN):
            AUX1 = 1.0 / (WING['XYZ'][j+1, 1] - Y) * (1.0 + (X - WING['XYZ'][j, 0]) / np.sqrt((X - WING['XYZ'][j, 0])**2 + (Y - WING['XYZ'][j+1, 1])**2))
            AUX2 = 1.0 / (Y - WING['XYZ'][j, 1]) * (1.0 + (X - WING['XYZ'][j, 0]) / np.sqrt((X - WING['XYZ'][j, 0])**2 + (Y - WING['XYZ'][j, 1])**2))
            
            b[i, j] = AUX1 + AUX2

        Db = WING['XYZ'][i+1, 1] - WING['XYZ'][i, 1]
        WInd = -1.0 / (4 * pi) * np.dot(b[i, :], GAMMA)
        D[i] = -DATA['Rho'] * WInd * Db * GAMMA[i]
        L[i] = DATA['Rho'] * DATA['VInf'] * GAMMA[i] * Db

    DD = np.sum(D)
    LL = np.sum(L)

    # CL and CD
    CL = LL / (0.5 * DATA['Rho'] * DATA['VInf']**2 * WING['Area'])
    CD = DD / (0.5 * DATA['Rho'] * DATA['VInf']**2 * WING['Area'])

    LOADS = {
        'D': D,
        'L': L,
        'DSum': DD,
        'LSum': LL,
        'CL': CL,
        'CD': CD
    }

    return LOADS

def PrintScreenReport(DATA):
    print('\n')
    print('******************************************************************************')
    print('                                   REPORT')
    print('******************************************************************************')
    print('Basic implementation of the lifting line theory')
    print('\n')
    print(f'Free-stream velocity: {DATA["VInf"]:8.6f}')
    print(f'Chord wing: {DATA["Chord"]:8.6f}')
    print(f'Air density: {DATA["Rho"]:8.6f}')
    print(f'Number of AoAs investigated: {len(DATA["AoA"]):4d}')
    print(f'Number of AEs (Aspect-Ratio) investigated: {len(DATA["AE"]):4d}')
    print(f'Number of different discretizations investigated: {len(DATA["NHShoe"]):4d}')
    print('-----------------------------------------------------------------------------')
    print('                            Aerodynamic Results')
    print('-----------------------------------------------------------------------------')
    print('Structure variable called AEROLOADS contains the following information:')
    print('AEROLOADS(i).LOADS: all results related to the test case "i"')
    print('AEROLOADS(i).LOADS.D: Induced drag per horseshoe')
    print('AEROLOADS(i).LOADS.L: Lift per horseshoe')
    print('AEROLOADS(i).LOADS.DSum: Total induced drag')
    print('AEROLOADS(i).LOADS.LSum: Total lift')
    print('AEROLOADS(i).LOADS.CL: Lift coefficient')
    print('AEROLOADS(i).LOADS.CD: Induced drag coefficient')


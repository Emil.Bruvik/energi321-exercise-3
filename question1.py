import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from myFunctions import *

plt.style.use('./custom_latex_style.mplstyle')

def biot_savart_mod(r, gamma=1, L=None, delta=0, epsilon=1e-8):
    if L is None:
        L = np.array([0,0,1])
    r_cross_L = np.cross(L, r)
    r_cross_L_norm_squared = np.linalg.norm(r_cross_L)**2
    L_norm_squared = np.linalg.norm(L)**2
    
    denominator = r_cross_L_norm_squared + (delta * L_norm_squared) + epsilon

    r_2 = r - L
    e_1 = r / np.linalg.norm(r)
    e_2 = r_2 / np.linalg.norm(r_2)
    v_mod = gamma / (4 * np.pi) * (r_cross_L / denominator) * (np.dot(L, e_1 - e_2))
    return v_mod

x_values = []
velocity_original = []
velocity_modified = []

delta = 0  
for x in np.arange(0, 5, 0.1):
    r = np.array([x, 0, 0.5])
    biot_original = biot_savart_mod(r, delta=delta)
    x_values.append(x)
    velocity_original.append(np.linalg.norm(biot_original))

delta = 0.05
for x in np.arange(0, 5, 0.1):
    r = np.array([x, 0, 0.5])
    biot_mod = biot_savart_mod(r, delta=delta)
    velocity_modified.append(np.linalg.norm(biot_mod))

fig, ax = plt.subplots(2, 1, figsize=(10, 16), sharex=True)

ax[0].plot(x_values, velocity_original, 'o-', color='b')
ax[0].set(title='Velocity induced by finite vortex segment')
ax[0].set(xlabel='Distance from point (b)', ylabel='Velocity')
ax[0].grid()

ax[1].plot(x_values, velocity_modified, 'o-', color='r')
ax[1].set(title='Modified velocity induced by finite vortex segment ($\delta = 0.05$)')
ax[1].set(xlabel='Distance from point (b)', ylabel='Velocity')
ax[1].grid()

plt.savefig('velocity_induced.pdf')
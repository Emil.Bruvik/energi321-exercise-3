import numpy as np
from myFunctions import *
import matplotlib.pyplot as plt
from scipy.stats import linregress
import seaborn as sns
sns.set_style('whitegrid')
plt.style.use('./custom_latex_style.mplstyle')

# Basic implementation of the lifting line theory
# Author: Dr. Bruno Roccia, Bergen Offshore Wind Centre, UiB
# Contact: bruno.roccia@uib.no
# Date: February 15, 2024

# INPUT DATA

DATA = {
    "VInf": 10.0,         # Free-stream velocity
    "Chord": 1.0,         # Chord wing dimension
    "Rho": 1.225,         # Air density
    "NHShoe": [50],       # Number of horseshoe elements
    "AE": [10],            # Aspect ratio
    "AoA": np.arange(0, 25, 1)  # Angle of attack in degrees
}

FLAG_PLOT = 2           # (1: PLOT Wing / 2: Plot Force curves)
FLAG_Analysis = 2       # Analysis flag

# Lengths
N1 = len(DATA["AoA"])
N2 = len(DATA["AE"])
N3 = len(DATA["NHShoe"])


# Analysis
if FLAG_Analysis == 1:
    WING = Geometry(DATA, 0, 0)
    AMatrix = AerodynamicMatrix(DATA, WING)
    RHS = RightHandSide(DATA, WING, 0)
    GAMMA = np.linalg.solve(AMatrix, RHS)
    LOADS = AerodynamicLoads(WING, DATA, GAMMA)

elif FLAG_Analysis == 2:
    AEROLOADS = [None] * N1
    for i in range(N1):
        WING = Geometry(DATA, 0, 0)
        AMatrix = AerodynamicMatrix(DATA, WING)
        RHS = RightHandSide(DATA, WING, i)
        GAMMA = np.linalg.solve(AMatrix, RHS)
        LOADS = AerodynamicLoads(WING, DATA, GAMMA)
        AEROLOADS[i] = {"LOADS": LOADS}

elif FLAG_Analysis == 3:
    AEROLOADS = [None] * N2
    for i in range(N2):
        WING = Geometry(DATA, i, 0)
        AMatrix = AerodynamicMatrix(DATA, WING)
        RHS = RightHandSide(DATA, WING, 0)
        GAMMA = np.linalg.solve(AMatrix, RHS)
        LOADS = AerodynamicLoads(WING, DATA, GAMMA)
        AEROLOADS[i] = {"LOADS": LOADS}

elif FLAG_Analysis == 4:
    AEROLOADS = [None] * N3
    for i in range(N3):
        WING = Geometry(DATA, 0, i)
        AMatrix = AerodynamicMatrix(DATA, WING)
        RHS = RightHandSide(DATA, WING, 0)
        GAMMA = np.linalg.solve(AMatrix, RHS)
        LOADS = AerodynamicLoads(WING, DATA, GAMMA)
        AEROLOADS[i] = {"LOADS": LOADS}

# Result prints
PrintScreenReport(DATA)

#%%



# Assuming AEROLOADS is a list of dictionaries with a similar structure to MATLAB's struct array
L = [aeroload['LOADS']['LSum'] for aeroload in AEROLOADS]
D = [aeroload['LOADS']['DSum'] for aeroload in AEROLOADS]
CL = [aeroload['LOADS']['CL'] for aeroload in AEROLOADS]
CD = [aeroload['LOADS']['CD'] for aeroload in AEROLOADS]

# Plotting Lift coefficient vs AoA
plt.figure(1)
plt.plot(DATA['AoA'], CL, color='b', linewidth=2)
plt.xlabel('Angle of Attack')
plt.ylabel('Lift coefficient C_L')
plt.title('Lift coefficient vs AoA')

# Plotting Induced Drag coefficient vs AoA
plt.figure(2)
plt.plot(DATA['AoA'], CD, color='b', linewidth=2)
plt.xlabel('Angle of Attack')
plt.ylabel('Induced Drag coefficient C_D')
plt.title('Induced drag coefficient vs AoA')

plt.show()

# Least squares CL/alpha

slope, intercept, r, pval, stderr = linregress(DATA['AoA'][:15], CL[:15])
leastsquares_model = intercept + slope * DATA['AoA'][:15]

fig, ax = plt.subplots()
ax.scatter(DATA['AoA'][:15], CL[:15], color="blue", label="Computed values")
ax.plot(DATA['AoA'][:15], leastsquares_model[:15], color="red", linestyle="--", label="Least squares fit")
ax.set(xlabel=r'Angle of attack $\alpha$', ylabel=r'Lift coefficient $C_L$')
polynomial_text = f'$C_L = {intercept:.2f} + {slope:.2f}\\alpha$'
ax.text(7.5, 0.1, polynomial_text, fontsize=16, bbox=dict(facecolor='white', alpha=0.5))
ax.legend()
ax.grid()

plt.savefig('least_squares.pdf')

# Least squares L/alpha

slope, intercept, r, pval, stderr = linregress(DATA['AoA'][:15], L[:15])
leastsquares_model = intercept + slope * DATA['AoA'][:15]

fig, ax = plt.subplots()
ax.scatter(DATA['AoA'][:15], L[:15], color="blue", label="Computed values")
ax.plot(DATA['AoA'][:15], leastsquares_model[:15], color="red", linestyle="--", label="Least squares fit")
ax.set(xlabel=r'Angle of attack $\alpha$', ylabel=r'Lift $L$')
polynomial_text = f'$L = {intercept:.2f} + {slope:.2f}\\alpha$'
ax.text(7.5, 0.1, polynomial_text, fontsize=16, bbox=dict(facecolor='white', alpha=0.5))
ax.legend()
ax.grid()

plt.savefig('least_squares_L.pdf')


